import axios from "axios";
import Api from "./Api";

export default class Users {

    constructor(id, device, keywords, page, source) {
        this.id = id
        this.device = device
        this.keywords = keywords
        this.page = page
        this.source = source
    }

    async Init(){
        return await axios.post(Api.API_BACKEND + '/api/init', {
            user: this.id,
            device: this.device,
            source: this.source,
            page: this.page,
            keywords: this.keywords
        })
    }


    async pointCheck(name, id){
        await axios.post(Api.API_BACKEND + '/api/points/check', {
                id_points: id,
                value: name
            })
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
            })
    }

    async getAllPoint(){
        await axios.get(Api.API_BACKEND + '/api/points')
            .then(res => {
                if (res.data.status){
                    let points = res.data.data
                    points.forEach((item) => {
                        switch (item.type) {
                            case 1:
                                this.pointCheck(item.name, item.id)
                                break;
                            case 2:
                                if (item.value === location.pathname){
                                    this.pointCheck(item.name, item.id)
                                }
                                break;
                            case 3:
                                document.querySelectorAll(item.selector).forEach((point) => {
                                    point.addEventListener('click', (e) => this.pointCheck(item.name, item.id))
                                })
                                break;
                            case 4:
                                document.querySelectorAll('a').forEach((point) => {
                                    point.addEventListener('click', (e) => {
                                        if (point.getAttribute('href').includes('tel')){
                                            this.pointCheck(item.name, item.id)
                                        }
                                    })
                                })
                                break;
                            case 5:
                                document.querySelectorAll('a').forEach((point) => {
                                    point.addEventListener('click', (e) => {
                                        if (point.getAttribute('href').includes('mailto')){
                                            this.pointCheck(item.name, item.id)
                                        }
                                    })
                                })
                                break;
                        }
                    })
                }
            })
    }



}

export function checkDevice(){
    if (!/Macintosh/i.test(navigator.userAgent)) {
        if (/Android/i.test(navigator.userAgent)) {
            return "android"
        }
        if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            return "iphone"
        }
        return "windows"
    } else {
        return "mac"
    }
}
export function getPageUrl(){
    if (location){
        return  location.origin + location.pathname
    }
}
export function getSource(){
    const params = new URL(location.href).searchParams;
    if (params){
        if (params.get('utm_source')){
            console.log(params.get('utm_source'))
            return  params.get('utm_source')
        } else {
            return null
        }
    } else {
        return null;
    }
}
export function getKeywords(){
    const params = new URL(location.href).searchParams;
    if (params){
        if (params.get('utm_term')){
            console.log(params.get('utm_term'))
            return  params.get('utm_term')
        } else {
            return null
        }
    } else {
        return null;
    }
}
export function checkUserId(){
    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    if (getCookie('userAnalytic') === undefined) {
        return undefined;
    }
    return getCookie('userAnalytic')
}
export async function getUserId(){
    return await axios.get(Api.API_BACKEND + '/api/getuserid')
}