import axios from "axios";
import Api from "./Api";

export default async (data) => {
    return await axios.post(Api.API_BACKEND + '/api/order/create', {
        name: data.name,
        phone: data.phone,
        info: data.info
    })
}