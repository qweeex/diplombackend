import {checkDevice, getSource, getKeywords, getPageUrl, checkUserId, getUserId} from "./modules/Users";
import Users from "./modules/Users";
import Orders from "./modules/Orders";

(async () => {
    // Объект пользователя
    window.space = null

    // Создаем экземпляр пользователя
    if (checkUserId() !== undefined) {
        window.space = await new Users(checkUserId(), checkDevice(), getKeywords(), getPageUrl(), getSource())
        if (window.space){
            console.log('User: ', space)
            space.Init()
                .then(res => {
                    console.log(`User ID: ${space.id} init in backend`)
                    // Получаем цели и ставим их
                    space.getAllPoint()
                })
        }
    } else {
        await getUserId()
            .then(async user => {
                console.log(user)
                if (user.data.status){
                    document.cookie = `userAnalytic=${user.data.userId}; max-age=99999999`;
                    window.space = await new Users(user.data.userId, checkDevice(), getKeywords(), getPageUrl(), getSource())
                    if (window.space){
                        console.log('User: ', space)
                        space.Init()
                            .then(res => {
                                console.log(`User ID: ${space.id} init in backend`)
                                // Получаем цели и ставим их
                                space.getAllPoint()
                            })
                    }
                }
            })
            .catch(err => {
                console.log('Err get user id', err)
            })
    }


    window.SendData = async (name, phone, info) => {
        await Orders({
            name: name,
            phone: phone,
            info: info
        })
    }






})();