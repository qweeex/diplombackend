const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: "development",
    entry: "./src/js/app.js",
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'metrika.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Metrika",
            filename: "index.html",
            template: 'src/index.html'
        })
    ]
}
