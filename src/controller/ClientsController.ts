import WebManager from "../manager/WebManager";
import MysqlManager from "../manager/MysqlManager";
import express from "express";


class ClientsController {

    public static async DisconnectedUser(user: any){
        console.log(user)
    }

    public static async InitUser(req: express.Request, res: express.Response){
        if (req.body){
            try {
                let UserUUID = Date.now().toString(36) + Math.random().toString(36).substr(2);
                let user = req.body
                await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO clients SET ?', {
                    'id_clients': user.user,
                    'device': user.device,
                    'source': user.source,
                    'page': user.page,
                    'keywords': user.keywords,
                    'created': new Date()
                }, (err: Error, results: any) => {
                    if (err){
                        console.log(err)
                    } else {
                        res.json({
                            status: true,
                            userId: UserUUID
                        })
                    }
                })
            } catch (e) {
                console.log(e)
                res.status(500)
                res.json({
                    status: false,
                    message: e
                })
            }
        }

    }

    public static async InitClients(message: any){
        if (message !== null){
            console.log('[Client Init]:  Init user ', message)
            await MysqlManager.Instance.MysqlPoolConnections.query('UPDATE `clients` SET `active` = ? WHERE  ?', [
                true, {clients_id: message}
            ], (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    console.log(`Uset: ${message} is active`)
                }
            })
        } else {
            console.log('[Client Init]:  Init new user')
            let UserUUID = this.GetUUID()
            await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO clients SET ?', {
                'clients_id': UserUUID,
                'active': true
            }, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    WebManager.Instance.io.emit('createuuid', UserUUID)
                }
            })
        }
    }


    public static async GetNewUsers(req: express.Request, res: express.Response){
        try {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `clients` WHERE `id_clients` IS NULL',
            (err: Error, results: any) => {
                if (err){
                    res.status(500)
                    res.json({
                        status: false,
                        message: err
                    })
                } else {
                    res.json({
                        status: true,
                        count: results.length
                    })
                }
            })
        } catch (e){
            res.status(500)
            res.json({
                status: false,
                message: e
            })
        }
    }

    public static async GetOldUsers(req: express.Request, res: express.Response){
        try {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `clients`',
                (err: Error, results: any) => {
                    if (err){
                        res.status(500)
                        res.json({
                            status: false,
                            message: err
                        })
                    } else {
                        res.json({
                            status: true,
                            count: results.length
                        })
                    }
                })
        } catch (e){
            res.status(500)
            res.json({
                status: false,
                message: e
            })
        }
    }

    public static GetUUID(): string{
        return Date.now().toString(36) + Math.random().toString(36).substr(2);
    }

    public static InitUUID(req: express.Request, res: express.Response){
        let id: string = Date.now().toString(36) + Math.random().toString(36).substr(2);
        res.json({
            status: true,
            userId: id
        })
    }

}

export default ClientsController