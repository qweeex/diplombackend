import express from "express";
import MysqlManager from "../manager/MysqlManager";

class AnalyticController {

    public static async PagesCount(req: express.Request, res: express.Response){
        let {metrikaID} = req.body
        if (metrikaID) {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT page, count(page) FROM clients GROUP BY page ORDER BY count(page) DESC LIMIT 8', metrikaID, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                    return res.json({
                        status: false,
                        message: "Что то пошло не так",
                        err
                    })
                } else {
                    let data: any = []
                    results.forEach((item: any) => {
                        data.push({
                            url: item.page,
                            count: item['count(page)']
                        })
                    })
                    return res.json({
                        status: true,
                        data
                    })
                }
            })
        }
    }

    public static async WordsCount(req: express.Request, res: express.Response){
        let {metrikaID} = req.body
        if (metrikaID) {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT keywords, count(keywords) FROM clients  GROUP BY keywords ORDER BY count(keywords) DESC LIMIT 8', metrikaID, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                    return res.json({
                        status: false,
                        message: "Что то пошло не так",
                        err
                    })
                } else {
                    let data: any = []
                    results.forEach((item: any) => {
                        data.push({
                            url: item.keywords,
                            count: item['count(keywords)']
                        })
                    })
                    return res.json({
                        status: true,
                        data
                    })
                }
            })
        }
    }

    public static async DeviceCount(req: express.Request, res: express.Response){
        let {metrikaID} = req.body
        if (metrikaID) {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT device, count(device) FROM clients  GROUP BY device ORDER BY count(device)', metrikaID, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                    return res.json({
                        status: false,
                        message: "Что то пошло не так",
                        err
                    })
                } else {
                    let data: any = []
                    let labels: any = []
                    results.forEach((item: any, i: any) => {
                        if (item.device != null){
                            labels.push(item.device)
                            data.push(item['count(device)'])
                        }
                    })
                    return res.json({
                        status: true,
                        result: {
                            data: data,
                            labels: labels
                        }
                    })
                }
            })
        }
    }

    public static async UsersCount(req: express.Request, res: express.Response){
        await MysqlManager.Instance.MysqlPoolConnections.query('SELECT `id_clients`, COUNT(`id_clients`), `created` FROM `clients` GROUP BY DATE(`created`)', (err: Error, results: any) => {
                if (err){
                    console.log(err)
                    return res.json({
                        status: false,
                        message: "Что то пошло не так",
                        err
                    })
                } else {
                    let data: any = []
                    let labels: any = []
                    results.forEach((item: any, i: any) => {
                        if (item.id_clients != null){
                            labels.push(new Date(item.created).toLocaleDateString())
                            data.push(item['COUNT(`id_clients`)'])
                        }
                    })
                    return res.json({
                        status: true,
                        result: {
                            data: data,
                            labels: labels
                        }
                    })
                }
            })
    }

    public static async SourceCount(req: express.Request, res: express.Response){
        let {metrikaID} = req.body
        if (metrikaID) {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT source, count(source) FROM clients GROUP BY source ORDER BY count(source)', metrikaID, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                    return res.json({
                        status: false,
                        message: "Что то пошло не так",
                        err
                    })
                } else {
                    let data: any = []
                    let labels: any = []
                    results.forEach((item: any, i: any) => {
                        if (item.source != null){
                            labels.push(item.source)
                            data.push(item['count(source)'])
                        }
                    })
                    return res.json({
                        status: true,
                        result: {
                            data: data,
                            labels: labels
                        }
                    })
                }
            })
        }
    }



}

export default AnalyticController