import express from "express";
import Users from "../models/UsersModel";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import {secret} from "../data/Config.json"
import { v4 as uuidv4 } from 'uuid';

class AuthController {

    public static async RegistrationUser(req: express.Request, res: express.Response){

        const {email, password, name, surname, website} = req.body

        if (email != '' && password != ''){
            try {
                await Users.findUser(email).then(async (currentUser) => {
                    if (currentUser.length === 0){
                        const hashPassword = bcrypt.hashSync(password, 10)
                        await Users.createUser({
                            email: email,
                            password: hashPassword,
                            name: name,
                            surname: surname,
                            website: website,
                            metrikaID: uuidv4()
                        })
                            .then((newUser) => {
                                return res.json({
                                    status: true,
                                    message: "Регистрация успешна",
                                    newUser
                                })
                            })
                    }
                })
            } catch (e) {
                console.error(e)
                return res.json({
                    status: false,
                    message: "Что то пошло не так...",
                    error: e
                })
            }
        } else {
            res.json({
                status: false,
                message: 'Username or password is empty'
            })
        }
    }

    public static async LoginUser(req: express.Request, res: express.Response){
        try {
            const {email, password} = req.body

            await Users.findUser(email)
                .then(user => {
                    if (user.length > 0){
                        const validPassword = bcrypt.compareSync(password, user[0].password)
                        if (!validPassword){
                            return res.json({
                                status: false,
                                message: "Ошибка пароля"
                            })
                        }
                        const token = jwt.sign({
                            id: user[0].id
                        }, secret, {
                            expiresIn: "10h"
                        })
                        return res.json({
                            status: true,
                            token: token,
                            metrikaID: user[0].metrikaID
                        })

                    } else {
                        return res.json({
                            status: false,
                            message: "Пользователь не найден"
                        })
                    }
                })
        } catch (e) {
            console.error(e)
            return res.json({
                status: false,
                error: e
            })
        }
    }


}

export default AuthController