import express from "express";
import PointsModels from "../models/PointsModels";

class PointsController {


    public static async CheckPoints(req: express.Request, res: express.Response){
        let data: {id_points: number; value: string} = req.body
        if (data.id_points && data.value){
            await PointsModels.Check(data)
                .then(result => {
                    return res.json({
                        status: true,
                        data: result
                    })
                })
                .catch(err => {
                    return res.status(500).json({
                        status: false,
                        err
                    })
                })
        } else {
            return res.json({
                status: false,
                data: "Data not found"
            })
        }

    }



    public static async AddPoints(req: express.Request, res: express.Response){
        let data: {
            name: string
            type: number
            value: string
        } = req.body

        if (data.name && data.type && data.value) {
            await PointsModels.Add(data)
                .then(result => {
                    return res.json({
                        status: true,
                        data: result
                    })
                })
                .catch(err => {
                    return res.status(500).json({
                        status: false,
                        err
                    })
                })
        } else {
            return res.json({
                status: false,
                data: "Data not found"
            })
        }

    }


    public static async RemovePoints(req: express.Request, res: express.Response){
        let data: {id: number} = req.body
        if (data.id) {
            await PointsModels.Remove(data.id)
                .then(result => {
                    return res.json({
                        status: true,
                        data: result
                    })
                })
                .catch(err => {
                    return res.status(500).json({
                        status: false,
                        err
                    })
                })
        } else {
            return res.json({
                status: false,
                data: "Data not found"
            })
        }
    }

    public static async ListPoints(req: express.Request, res: express.Response){

        await PointsModels.List()
            .then(result => {
                return res.json({
                    status: true,
                    data: result
                })
            })
            .catch(err => {
                return res.status(500).json({
                    status: false,
                    err
                })
            })

    }


    public static async GetTypesPoints(req: express.Request, res: express.Response){
        await PointsModels.Types()
            .then(result => {
                return res.json({
                    status: true,
                    data: result
                })
            })
            .catch(err => {
                return res.status(500).json({
                    status: false,
                    err
                })
            })
    }

}

export default PointsController