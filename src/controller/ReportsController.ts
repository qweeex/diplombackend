import express from "express";
import MysqlManager from "../manager/MysqlManager";

class ReportsController {

    public static async PointerReports(req: express.Request, res: express.Response) {
        let {metrikaID, idPoint} = req.body
        if (idPoint) {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT `id_points`, count(`id_points`),`created` FROM points_callback WHERE `id_points` = ? GROUP BY DATE(`created`)', idPoint, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                    return res.json({
                        status: false,
                        message: "Что то пошло не так",
                        err
                    })
                } else {
                    let data: any = []
                    results.forEach((item: any) => {
                        data.push({
                            time: item.created,
                            count: item['count(`id_points`)']
                        })
                    })
                    return res.json({
                        status: true,
                        data: data
                    })
                }
            })
        }
    }

}

export default ReportsController