import express from "express";
import OrderModels from "../models/OrderModels";

export default class OrderController {

    public static async AddOrder(req: express.Request, res: express.Response) {
        let data: {
            name: string
            phone: string
            info: object
        } = req.body

        if (data.name && data.phone){

            await OrderModels.Add(data)
                .then(result => {
                    return res.json({
                        status: true,
                        data: result
                    })
                })
                .catch(err => {
                    return res.status(500).json({
                        status: false,
                        err
                    })
                })

        } else {
            return res.json({
                status: false,
                message: "Data not found"
            })
        }
    }

    public static async List(req: express.Request, res: express.Response) {

        await OrderModels.List()
                .then(result => {
                    return res.json({
                        status: true,
                        data: result
                    })
                })
                .catch(err => {
                    return res.status(500).json({
                        status: false,
                        err
                    })
                })

    }

}