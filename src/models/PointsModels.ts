
import MysqlManager from "../manager/MysqlManager";

interface PointsAdd {
    name: string
    type: number
    value: string
}

export default  class PointsModels {

    public static async List(){
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `points`',
                (err: Error, result: any) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(result)
                })
        })
    }

    public static async Types(){
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `points_type`',
                (err: Error, result: any) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(result)
                })
        })
    }

    public static async Add(point: PointsAdd){
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO points SET ?', point,
                (err: Error, result: any) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(result)
                })
        })
    }

    public static async Remove(id: number){
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('DELETE FROM points WHERE id = ?', id,
                (err: Error, result: any) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(result)
                })
        })
    }

    public static async Check(point: {id_points: number; value: string}){
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO points_callback SET ?', {
                    id_points: point.id_points,
                    value: point.value,
                    created: new Date()
                }, (err: Error, result: any) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(result)
                })
        })
    }

}

