import MysqlManager from "../manager/MysqlManager";

interface User {
    email: string;
    password: string;
    name: string;
    surname: string;
    website: string;
    metrikaID: string;
}

class Users {

    public static async findUser(email: string): Promise<any> {
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query({
                sql: 'SELECT * FROM `users` WHERE `email` = ?',
                values: email
            }, (err: Error, user: any) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(user)
                }
            })
        })
    }


    public static async createUser(user: User): Promise<any>{
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO users SET ?', user, (err: Error, results: any) => {
                if (err){
                    reject(err)
                } else {
                    resolve(results)
                }
            })
        })
    }

}
export default Users