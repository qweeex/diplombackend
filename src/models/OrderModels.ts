import MysqlManager from "../manager/MysqlManager";

interface AddOrderStruct {
    name: string
    phone: string
    info: object
}

export default class OrderModels {

    public static async Add(order: AddOrderStruct){
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO orders SET ?', {
                name: order.name,
                phone: order.phone,
                info: JSON.stringify(order.info),
                created: new Date()
            }, (err: Error, result: any) => {
                if (err) reject(err);
                resolve(result)
            })
        })
    }

    public static async List(){
        return new Promise<any>(async (resolve, reject) => {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `orders`',
                (err: Error, result: any) => {
                    if (err) reject(err);
                    resolve(result)
                })
        })
    }

}