import express from "express"
import * as core from "express-serve-static-core"
import { createServer } from "http";
import { Server } from "socket.io";
import cors from "cors"

import ClientsController from "../controller/ClientsController";
import PointsController from "../controller/PointsController";
import OrderController from "../controller/OrderController";
import AuthController from "../controller/AuthController";
import AnalyticController from "../controller/AnalyticController";
import ReportsController from "../controller/ReportsController";

class WebManager {
    public static readonly Instance: WebManager = new WebManager()
    public PORT:number = 8085;
    private readonly App!: core.Express
    public io: Server
    private server!: any

    private constructor() {
        this.App = express()
        this.App.use(express.json())
        this.App.use(cors())
        this.server = createServer(this.App)
        this.io = new Server(this.server, {
            cors: {
                origin: '*',
            }
        })

        this.ApiRouter()
    }

    private ApiRouter(): void {

        this.App.post('/api/auth/login', AuthController.LoginUser)
        this.App.post('/api/auth/registration', AuthController.RegistrationUser)

        this.App.get('/api/getuserid', ClientsController.InitUUID)
        this.App.post('/api/init', ClientsController.InitUser)


        this.App.post('/api/points/check', PointsController.CheckPoints)
        this.App.post('/api/points/add', PointsController.AddPoints)
        this.App.post('/api/points/remove', PointsController.RemovePoints)
        this.App.get('/api/points', PointsController.ListPoints)
        this.App.get('/api/points/types', PointsController.GetTypesPoints)


        this.App.post('/api/order/create', OrderController.AddOrder)
        this.App.get('/api/order/list', OrderController.List)


        this.App.get('/api/old-users', ClientsController.GetOldUsers)
        this.App.get('/api/new-users', ClientsController.GetNewUsers)


        this.App.post('/api/device-analytic', AnalyticController.SourceCount)
        this.App.post('/api/source-analytic', AnalyticController.DeviceCount)
        this.App.post('/api/pages-analytic', AnalyticController.PagesCount)
        this.App.post('/api/words-analytic', AnalyticController.WordsCount)
        this.App.post('/api/users-analytic', AnalyticController.UsersCount)
        this.App.post('/api/reports/pointer', ReportsController.PointerReports)

    }

    public Start(): void{
        this.server.listen(this.PORT, () => {
            console.log(`Server start on port ${this.PORT}`)
        })

        this.io.on("connect", (socket: any) => {
            console.log("Connected client on port %s.", this.PORT);
            socket.on("message", (m:any) => {
                console.log("[server](message): %s", JSON.stringify(m));
                this.io.emit("message", m);
            });

            socket.on('init', (m: any) => ClientsController.InitClients(m))

            socket.on("disconnect", () => {
                console.log("Client disconnected");
            });
        });
    }

}

export default WebManager